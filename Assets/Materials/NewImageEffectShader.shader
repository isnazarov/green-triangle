﻿Shader "Hidden/NewImageEffectShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_SecondTex("Second Texture", 2D) = "white" {}
		_Tween("Tween", Range(0, 1)) = 0
		//_Color("Color", Color) = (1,1,1,1)

		_GridTex("Grid Texture", 2D) = "white" {}
		_NoiseTex("Noise Texture", 2D) = "white" {}
		_DistortionDamper("Distortion Damper", Float) = 10
		_DistortionSpreader("Distortion Spreader", Float) = 100
		_TimeDamper("Time Damper", Float) = 20
    }

    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

		Tags 
		{
			"Queue" = "Transparent"
		}

        Pass
        {
			//Blend SrcAlpha OneMinusSrcAlpha
			Blend One One

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
			sampler2D _SecondTex;
			sampler2D _GridTex;
			sampler2D _NoiseTex;
			float _Tween;
			float _DistortionDamper;
			float _DistortionSpreader;
			float _TimeDamper;

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * _Tween;
				fixed4 col1 = tex2D(_SecondTex, i.uv) * (1 - _Tween);
                // just invert the colors
                //col.rgb = 1 - col.rgb;
                //return col * fixed4(i.uv.r, i.uv.g, 0, 1);
				return col + col1 ;
				//return fixed4(i.uv.r, i.uv.g, 1, 1);
            }
            ENDCG
        }
    }
}
