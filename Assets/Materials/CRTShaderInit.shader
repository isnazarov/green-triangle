﻿Shader "CustomRenderTexture/CRTShaderInit"
{
	Properties{

		_Texture("Init Texture", 2D) = "white" {}

	}

	SubShader
	{
		//Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Lighting Off
		Blend One Zero

		Pass
		{
			HLSLPROGRAM
			#include "UnityCustomRenderTexture.cginc"
			#pragma vertex InitCustomRenderTextureVertexShader
			#pragma fragment frag
			#pragma target 3.0

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x


			sampler2D _Texture;

			float4 frag(v2f_init_customrendertexture IN) : COLOR
			{
				
				/*float4 col = tex2D(_Texture, IN.texcoord.xy);
				if (col.a > 0.1) 
				{
					col = float4(1,1,.3,1);
				}
				return col;*/

				return tex2D(_Texture, IN.texcoord.xy);
			}
			ENDHLSL
		}
	}
}