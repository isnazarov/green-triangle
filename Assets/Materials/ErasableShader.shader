﻿// !!!!!!
// To work on mobile custom shaders should be included in ProjectSettings/Graphics/Always included shaders 
// !!!!!!
Shader "CustomRenderTexture/Erasable"
{
	Properties
	{
		//_MainTex("Sprite Texture", 2D) = "white" {}
		_DrawPosition("Draw Position", Vector) = (-1, -1, 0, 0)
		
	}

		SubShader
	{
		Lighting Off
		Blend One Zero

		Pass
		{
			CGPROGRAM

			#include "UnityCustomRenderTexture.cginc"
			//#include "UnityCG.cginc"
			#pragma vertex CustomRenderTextureVertexShader
			#pragma fragment frag
			#pragma target 3.0

			//#pragma prefer_hlslcc gles
			//#pragma exclude_renderers d3d11_9x

			float4		_DrawPosition;
			float4      _Color;
			sampler2D   _Tex;
			

			
			float4 frag(v2f_customrendertexture IN) : COLOR
			{
				float4 outputColor = tex2D(_SelfTexture2D, IN.localTexcoord.xy);
				
				if (distance(IN.localTexcoord.xy, _DrawPosition) < .1)
				{
					outputColor.a = 0;
				}
				return outputColor;
				
				//return float4(.3,1,.1,1);
			}
			
			ENDCG
		}
		
	}
}