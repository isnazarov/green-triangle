﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleCompute : MonoBehaviour
{
    public ComputeShader computeShader;

    public RenderTexture result;

    //public static readonly int Result = Shader.id

    // Start is called before the first frame update
    void Start()
    {
        int kernel = computeShader.FindKernel("CSMain");

        result = new RenderTexture(512, 512, 24);
        result.enableRandomWrite = true;
        result.Create();

        computeShader.SetTexture(kernel, "Result", result);
        computeShader.Dispatch(kernel, 512/8, 512/8, 1);
        computeShader.SetVector("Color", new Vector4(0f, 1f, 0f, 1f));
    }

    // Update is called once per frame
    void Update()
    {
        computeShader.SetVector("Color", new Vector4(0f, 1f, 0f, 1f));
        //computeShader.setb
    }
}
