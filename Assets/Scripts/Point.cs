﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class Point : MonoBehaviour
{
    public static event Action OnPositionChange;
    public static event Action OnPointStartDrag;
    public static event Action OnPointStopDrag;

    private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void OnMouseDrag()
    {
        Vector3 newPos = _camera.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
        OnPositionChange?.Invoke();
    }

    private void OnMouseDown()
    {
        OnPointStartDrag?.Invoke();
    }

    private void OnMouseUp()
    {
        OnPointStopDrag?.Invoke();
    }


}
