using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;


[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]  // REQUIRES A COLLIDER2D to function
// 1. Attach this to a read/write enabled sprite image
// 2. Set the drawing_layers  to use in the raycast
// 3. Attach a 2D collider (like a Box Collider 2D) to this sprite
// 4. Hold down left mouse to draw on this texture!
public class Drawable : MonoBehaviour
{
    public Texture2D _sourceTexture;

    [Space]
    public Transform _point0;
    public Transform _point1;

    [Space]
    public ParticleSystem _winParticles;

    // PEN COLOUR
    public static Color Pen_Colour = Color.clear;     // Change these to change the default drawing settings
    // PEN WIDTH (actually, it's a radius, in pixels)
    public static int Pen_Width = 25;


    public delegate void Brush_Function(Vector2 world_position);
    // This is the function called when a left click happens
    // Pass in your own custom one to change the brush type
    // Set the default function in the Awake method
    public Brush_Function current_brush;

    public LayerMask Drawing_Layers;

    // Used to reference THIS specific file without making all methods static
    public static Drawable drawable;

    // MUST HAVE READ/WRITE enabled set in the file editor of Unity
    Sprite drawable_sprite;
    Texture2D drawable_texture;
    private BoxCollider2D _collider;
    private SpriteRenderer _spriteRenderer;
    private RenderTexture _inputTexture;

    private uint[] _bufferData;
    private int kernelCSCheckTransparency;
    private bool _isTransparentOverLine = false;
    private bool _isInputReady = true;
    Vector2 _point0textureCoord;
    Vector2 _point1textureCoord;

    Vector2 previous_drag_position;
    Color[] clean_colours_array;
    Color transparent;
    Color32[] cur_colors;
    bool mouse_was_previously_held_down = false;
    bool no_drawing_on_current_drag = false;



//////////////////////////////////////////////////////////////////////////////
// BRUSH TYPES. Implement your own here


    // Pass in a point in WORLD coordinates
    // Changes the surrounding pixels of the world_point to the static pen_colour
    public void PenBrush(Vector2 world_point)
    {
        Vector2 pixel_pos = WorldToPixelCoordinates(world_point);

        cur_colors = drawable_texture.GetPixels32();

        if (previous_drag_position == Vector2.zero)
        {
            // If this is the first time we've ever dragged on this image, simply colour the pixels at our mouse position
            MarkPixelsToColour(pixel_pos, Pen_Width, Pen_Colour);
        }
        else
        {
            // Colour in a line from where we were on the last update call
            //!!! This strongly reduces performance on mobile. So we just avoid using it and it looks ok.
            //ColourBetween(previous_drag_position, pixel_pos, Pen_Width, Pen_Colour);

            MarkPixelsToColour(pixel_pos, Pen_Width, Pen_Colour);
        }
        ApplyMarkedPixelChanges();

        previous_drag_position = pixel_pos;
    }


    // Helper method used by UI to set what brush the user wants
    // Create a new one for any new brushes you implement
    public void SetPenBrush()
    {
        // PenBrush is the NAME of the method we want to set as our current brush
        current_brush = PenBrush;
    }
//////////////////////////////////////////////////////////////////////////////






    // This is where the magic happens.
    // Detects when user is left clicking, which then call the appropriate function
    void Update()
    {
        if (_isInputReady)
        {
            // Is the user holding down the left mouse button?
            bool mouse_held_down = Input.GetMouseButton(0);
            if (mouse_held_down && !no_drawing_on_current_drag)
            {
                // Convert mouse coordinates to world coordinates
                Vector2 mouse_world_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                // Check if the current mouse position overlaps our image
                Collider2D hit = Physics2D.OverlapPoint(mouse_world_position, Drawing_Layers.value);
                if (hit != null && hit.transform != null)
                {
                    // We're over the texture we're drawing on!
                    // Use whatever function the current brush is
                    current_brush(mouse_world_position);

                    ComputeOpacityOverLine();
                    if (_isTransparentOverLine)
                    {
                        Win();
                    }
                }

                else
                {
                    // We're not over our destination texture
                    previous_drag_position = Vector2.zero;
                    if (!mouse_was_previously_held_down)
                    {
                        // This is a new drag where the user is left clicking off the canvas
                        // Ensure no drawing happens until a new drag is started
                        no_drawing_on_current_drag = true;
                    }
                }
            }
            // Mouse is released
            else if (!mouse_held_down)
            {
                previous_drag_position = Vector2.zero;
                no_drawing_on_current_drag = false;
                ResetCanvas();
            }
            mouse_was_previously_held_down = mouse_held_down;
        }
        
    }



    // Set the colour of pixels in a straight line from start_point all the way to end_point, to ensure everything inbetween is coloured
    public void ColourBetween(Vector2 start_point, Vector2 end_point, int width, Color color)
    {
        // Get the distance from start to finish
        float distance = Vector2.Distance(start_point, end_point);
        Vector2 direction = (start_point - end_point).normalized;

        Vector2 cur_position = start_point;

        // Calculate how many times we should interpolate between start_point and end_point based on the amount of time that has passed since the last update
        float lerp_steps = 1 / distance;

        for (float lerp = 0; lerp <= 1; lerp += lerp_steps)
        {
            cur_position = Vector2.Lerp(start_point, end_point, lerp);
            MarkPixelsToColour(cur_position, width, color);
        }
    }





    public void MarkPixelsToColour(Vector2 center_pixel, int pen_thickness, Color color_of_pen)
    {
        // Figure out how many pixels we need to colour in each direction (x and y)
        int center_x = (int)center_pixel.x;
        int center_y = (int)center_pixel.y;

        for (int x = center_x - pen_thickness; x <= center_x + pen_thickness; x++)
        {
            // Check if the X wraps around the image, so we don't draw pixels on the other side of the image
            if (x >= (int)drawable_sprite.rect.width || x < 0)
                continue;

            for (int y = center_y - pen_thickness; y <= center_y + pen_thickness; y++)
            {
                if (Mathf.Pow(x - center_x, 2) + Mathf.Pow(y - center_y, 2) <= pen_thickness * pen_thickness) // Make pen round
                    MarkPixelToChange(x, y, color_of_pen);
            }
        }
    }
    public void MarkPixelToChange(int x, int y, Color color)
    {
        // Need to transform x and y coordinates to flat coordinates of array
        int array_pos = y * (int)drawable_sprite.rect.width + x;

        // Check if this is a valid position
        if (array_pos >= cur_colors.Length || array_pos < 0)
            return;

        cur_colors[array_pos] = color;
    }
    public void ApplyMarkedPixelChanges()
    {
        drawable_texture.SetPixels32(cur_colors);
        drawable_texture.Apply();
    }


    // Directly colours pixels. This method is slower than using MarkPixelsToColour then using ApplyMarkedPixelChanges
    // SetPixels32 is far faster than SetPixel
    // Colours both the center pixel, and a number of pixels around the center pixel based on pen_thickness (pen radius)
    public void ColourPixels(Vector2 center_pixel, int pen_thickness, Color color_of_pen)
    {
        // Figure out how many pixels we need to colour in each direction (x and y)
        int center_x = (int)center_pixel.x;
        int center_y = (int)center_pixel.y;

        for (int x = center_x - pen_thickness; x <= center_x + pen_thickness; x++)
        {
            for (int y = center_y - pen_thickness; y <= center_y + pen_thickness; y++)
            {
                drawable_texture.SetPixel(x, y, color_of_pen);
            }
        }

        drawable_texture.Apply();
    }


    public Vector2 WorldToPixelCoordinates(Vector2 world_position)
    {
        // Change coordinates to local coordinates of this image
        Vector3 local_pos = transform.InverseTransformPoint(world_position);

        // Change these to coordinates of pixels
        float pixelWidth = drawable_sprite.rect.width;
        float pixelHeight = drawable_sprite.rect.height;
        float unitsToPixels = pixelWidth / drawable_sprite.bounds.size.x * transform.localScale.x;

        // Need to center our coordinates
        float centered_x = local_pos.x * unitsToPixels + pixelWidth / 2;
        float centered_y = local_pos.y * unitsToPixels + pixelHeight / 2;

        // Round current mouse position to nearest pixel
        Vector2 pixel_pos = new Vector2(Mathf.RoundToInt(centered_x), Mathf.RoundToInt(centered_y));

        return pixel_pos;
    }


    // Changes every pixel to be the reset colour
    public void ResetCanvas()
    {
        _spriteRenderer.enabled = true;

        drawable_texture = new Texture2D(_sourceTexture.width, _sourceTexture.height, TextureFormat.RGBA32, false);

        // This doesn't work on mobile!
        //Graphics.CopyTexture(_sourceTexture, drawable_texture);
        //
        // Instead:
        drawable_texture.SetPixels32(_sourceTexture.GetPixels32());
        drawable_texture.Apply();

        // Sprites created from scripts are always read/write enabled
        _spriteRenderer.sprite = Sprite.Create(drawable_texture,
            new Rect(0,0, drawable_texture.width, drawable_texture.height),
            new Vector2(0.5f, 0.5f),
            100
            );

        drawable_sprite = _spriteRenderer.sprite;

        cur_colors = drawable_texture.GetPixels32();

        _collider.size = drawable_sprite.rect.size / drawable_sprite.pixelsPerUnit;

        UpdatePointsPosition();
        _isInputReady = true;
    }


        
    void Awake()
    {
        drawable = this;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _collider = GetComponent<BoxCollider2D>();

        // DEFAULT BRUSH SET HERE
        current_brush = PenBrush;

        ResetCanvas();

        Point.OnPositionChange += ComputeOpacityOverLine;
        Point.OnPointStartDrag += () => _isInputReady = false;
        Point.OnPointStopDrag += () => _isInputReady = true;
        
    }
    

    private void ComputeOpacityOverLine()
    {
        UpdatePointsPosition();

        bool opacity = false;
        int width = (int)drawable_sprite.rect.width;
        int height = (int)drawable_sprite.rect.height;

        for (int x = 0; x < (int)drawable_sprite.rect.width; x++)
        {
            for (int y = FindY(x); y < (int)drawable_sprite.rect.height; y++)
            {
                if (cur_colors[width * y + x].a > 5)
                {
                    opacity = true;
                    break;
                }
            }
            if (opacity) break;
        }
        _isTransparentOverLine = !opacity;
    }

    int FindY(int x)
    {
        float k = (_point0textureCoord.y - _point1textureCoord.y) / (_point0textureCoord.x - _point1textureCoord.x);
        float b = _point0textureCoord.y - (k * _point0textureCoord.x);
        return (int)((k * x) + b);
    }

    private void UpdatePointsPosition()
    {
        float pixelsPerUnit = drawable_sprite.pixelsPerUnit;
        Vector2 scaleUnits = new Vector2(drawable_texture.width / pixelsPerUnit, drawable_texture.height / pixelsPerUnit);
        Vector2 extendsUnits = new Vector2(scaleUnits.x / 2, scaleUnits.y / 2);

        Vector2 vertex00Units = new Vector2(transform.position.x - extendsUnits.x, transform.position.y - extendsUnits.y);

        Vector2 point0normalized = new Vector2(
            InverseLerpUnclamped(vertex00Units.x, vertex00Units.x + scaleUnits.x, _point0.position.x),
            InverseLerpUnclamped(vertex00Units.y, vertex00Units.y + scaleUnits.y, _point0.position.y)
            );
        Vector2 point1normalized = new Vector2(
            InverseLerpUnclamped(vertex00Units.x, vertex00Units.x + scaleUnits.x, _point1.position.x),
            InverseLerpUnclamped(vertex00Units.y, vertex00Units.y + scaleUnits.y, _point1.position.y)
            );

        _point0textureCoord = new Vector2(
            Mathf.LerpUnclamped(0f, drawable_texture.width, point0normalized.x),
            Mathf.LerpUnclamped(0f, drawable_texture.height, point0normalized.y)
            );
        _point1textureCoord = new Vector2(
            Mathf.LerpUnclamped(0f, drawable_texture.width, point1normalized.x),
            Mathf.LerpUnclamped(0f, drawable_texture.height, point1normalized.y)
            );

    }

    private float InverseLerpUnclamped(float a, float b, float value)
    {
        return (value - a) / (b - a);
    }

    private void Win()
    {
        _isInputReady = false;
        _spriteRenderer.enabled = false;
        StartCoroutine(WinSequence());
    }

    private IEnumerator WinSequence()
    {
        _winParticles.Play();
        yield return new WaitForSecondsRealtime(3);
        ResetCanvas();
        yield return null;
    }

}
