﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Brush : MonoBehaviour
{
    public CustomRenderTexture _drawableTexture;
    public Material _drawableMaterial;

    public ComputeShader _computeShader;
    public RenderTexture _inputTexture;
    //public RenderTexture _result;

    [Space]
    public Transform _point0;
    public Transform _point1;

    [Space]
    public ParticleSystem _winParticles;
    public TMP_Text _debugTextField;

    private bool _isTransparentOverLine = false;
    private bool _isInputReady = true;

    private Camera _camera;
    private Material _material;
    private MeshRenderer _meshRenderer;

    private ComputeBuffer _computeBuffer;
    private uint[] _bufferData;
    private int kernelCSMain;
    private int kernelCSCheckTransparency;

    private static readonly int DrawPosition = Shader.PropertyToID("_DrawPosition");
    private static readonly int IsCleanOverLine = Shader.PropertyToID("_IsCleanOverLine");

    private void Awake()
    {
        _camera = Camera.main;
        _meshRenderer = GetComponent<MeshRenderer>();
        _material = _meshRenderer.sharedMaterial;
        
        InitComputeShader();

        Point.OnPositionChange += ComputeOpacityOverLine;
    }

    void Start()
    {
        Restart();
    }

    void Update()
    {
        if (_isInputReady)
        {
            if (Input.GetMouseButtonUp(0))
            {
                Restart();
            }
            if (Input.GetMouseButton(0))
            {
                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    _drawableMaterial.SetVector(DrawPosition, hit.textureCoord);
                    _drawableTexture.Update();
                    //_debugTextField.text = hit.textureCoord.ToString();

                    ComputeOpacityOverLine();
                    if (_isTransparentOverLine)
                    {
                        Win();
                    }
                }
            }
        }
        
    }

    private void OnDisable()
    {
        _computeBuffer.Dispose();
    }

    private void Restart()
    {
        _meshRenderer.enabled = true;
        _drawableMaterial.SetVector(DrawPosition, new Vector4(-1f, -1f));
        _drawableTexture.Initialize();

        UpdatePointsPosition();

        _isInputReady = true;
    }

    private void Win()
    {
        _isInputReady = false;
        _meshRenderer.enabled = false;
        StartCoroutine(WinSequence());
    }

    private IEnumerator WinSequence()
    {
        _winParticles.Play();
        yield return new WaitForSecondsRealtime(3);
        Restart();
        yield return null;
    }

    private void ComputeOpacityOverLine()
    {
        Graphics.Blit(_drawableTexture, _inputTexture);
        _computeShader.SetTexture(kernelCSCheckTransparency, "InTexture", _inputTexture);
        //_computeShader.SetTexture(kernelCSCheckTransparency, "Result", _result);
        UpdatePointsPosition();

        _computeShader.Dispatch(kernelCSCheckTransparency, (_inputTexture.width + 7) / 8, (_inputTexture.height + 7) / 8, 1);
        _computeBuffer.GetData(_bufferData);

        uint opacity = 0;
        //string debugBuff = "buffer:";
        for (int i = 0; i < _bufferData.Length; i++)
        {
            opacity |= _bufferData[i];
            //debugBuff += "," + _bufferData[i];
        }
        //Debug.Log("opacity=" + opacity + "  " + debugBuff);

        _isTransparentOverLine = opacity == 0;
    }

    private void InitComputeShader()
    {
        kernelCSMain = _computeShader.FindKernel("CSMain");

        int inputTextureWidth = _drawableTexture.initializationTexture.width;
        int inputTextureHeight = _drawableTexture.initializationTexture.height;
        _bufferData = new uint[((inputTextureWidth + 7) / 8) * ((inputTextureHeight + 7) / 8)];

        _inputTexture = new RenderTexture(inputTextureWidth, inputTextureHeight, 24);
        //_inputTexture.enableRandomWrite = true;
        _inputTexture.Create();

        //_result = new RenderTexture(inputTextureWidth, inputTextureHeight, 24);
        //_result.enableRandomWrite = true;
        //_result.Create();

        kernelCSCheckTransparency = _computeShader.FindKernel("CSCheckTransparency");
        //_computeShader.SetTexture(kernelCSCheckTransparency, "Result", _result);

        _computeBuffer = new ComputeBuffer(_bufferData.Length, 4);
        _computeBuffer.SetData(_bufferData);
        
        _computeShader.SetBuffer(kernelCSCheckTransparency, "dataBuffer", _computeBuffer);

        _computeShader.SetInt("InTextureWidth", inputTextureWidth);

    }

    private void UpdatePointsPosition()
    {
        Vector2 extendsUnits = new Vector2(transform.localScale.x / 2, transform.localScale.y / 2);
        Vector2 vertex00Units = new Vector2(transform.position.x - extendsUnits.x, transform.position.y - extendsUnits.y);

        Vector2 point0normalized = new Vector2(
            InverseLerpUnclamped(vertex00Units.x, vertex00Units.x + transform.localScale.x, _point0.position.x),
            InverseLerpUnclamped(vertex00Units.y, vertex00Units.y + transform.localScale.y, _point0.position.y)
            );
        Vector2 point1normalized = new Vector2(
            InverseLerpUnclamped(vertex00Units.x, vertex00Units.x + transform.localScale.x, _point1.position.x),
            InverseLerpUnclamped(vertex00Units.y, vertex00Units.y + transform.localScale.y, _point1.position.y)
            );

        Vector2 point0textureCoord = new Vector2(
            Mathf.LerpUnclamped(0f, _drawableTexture.initializationTexture.width, point0normalized.x),
            Mathf.LerpUnclamped(0f, _drawableTexture.initializationTexture.height, point0normalized.y)
            );
        Vector2 point1textureCoord = new Vector2(
            Mathf.LerpUnclamped(0f, _drawableTexture.initializationTexture.width, point1normalized.x),
            Mathf.LerpUnclamped(0f, _drawableTexture.initializationTexture.height, point1normalized.y)
            );

        _computeShader.SetFloat("x1", point0textureCoord.x);
        _computeShader.SetFloat("y1", point0textureCoord.y);
        _computeShader.SetFloat("x2", point1textureCoord.x);
        _computeShader.SetFloat("y2", point1textureCoord.y);
        
    }

    private float InverseLerpUnclamped(float a, float b, float value)
    {
        return (value - a) / (b - a);
    }
}
