﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    public Point p0;
    public Point p1;

    private LineRenderer _lineRenderer;

    private void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdatePosition();
        Point.OnPositionChange += UpdatePosition;
    }

    private void UpdatePosition()
    {
        _lineRenderer.SetPosition(0, p0.transform.position);
        _lineRenderer.SetPosition(1, p1.transform.position);
    }
}
